# Guía III UI

## Descripción
* **El presente repositorio "Guía III UI" contiene tres carpetas con los ejercicios de la guía, en cada carpeta existe su propia documentación, tanto en formato de REAME.md como comentarios en el código.**

### Para fácil acceso se agregan link directos a cada ejercicio junto a su documentación respectiva.

- [ ] [Ejercicio 1](https://gitlab.com/Dante1991HD/guia3/-/tree/main/ejercicio1) [README.md](https://gitlab.com/Dante1991HD/guia3/-/blob/main/ejercicio1/README.md)

- [ ] [Ejercicio 2](https://gitlab.com/Dante1991HD/guia3/-/tree/main/ejercicio2) [README.md](https://gitlab.com/Dante1991HD/guia3/-/blob/main/ejercicio2/README.md)

- [ ] [Ejercicio 3](https://gitlab.com/Dante1991HD/guia3/-/tree/main/ejercicio3) [README.md](https://gitlab.com/Dante1991HD/guia3/-/blob/main/ejercicio3/README.md)

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl


