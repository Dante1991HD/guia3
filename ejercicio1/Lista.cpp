#include <iostream>
#include "Lista.h"
using namespace std;
/* Constructor por defecto de la clase, primero y 
    último atributos de clase de tipo estructura nodo, 
    consta de un dato, en este caso int, y un puntero a la misma estructura */
Lista::Lista() {
    Nodo * primero = NULL;
    Nodo * ultimo = NULL;
}
/* Método que crea entradas a la lista dinámica ordenada.
    primeramente crea el nodo, le pasa el entero la variable dato
    y refencia el puntero que se está creando al puntero con el 
    que se ligará y así creará un nuevo nodo según el usuario lo requiera */
void Lista::crear(int numero) {
    /* Creamos un "Nodo" temporal para contener los datos que pasaremos a los
    atributos de clase según ciertas condiciones, si es el primero en la lista
    pasa el "Nodo" temporal al atributo, de lo contrario mientras no estemos 
    en el último "Nodo" de la lista los poscicionará en orden ascendente */
    Nodo * tmp = new Nodo;
    tmp -> dato = numero;
    tmp -> siguiente = NULL;
    /* Si el valor del primero es NULL significa que es el primero dato 
    ingresado en en la lista, por lo que no hay con qué compararlo, por 
    lo que se ingresa directamente */
    if (this -> primero == NULL) {
        this -> primero = tmp;
    } else {
        /* De modo contrario se crearán variables temporales para ayudar
        al intercambio de posición, su tipo de dato es "Nodo", estructura
        descrita anteriormente, "actual" contendrá el valor a comparar dentro
        del ciclo e intercambiar con "Siguiente" */
        Nodo * actual, * siguiente;
        siguiente = this -> primero;
        /* Se conprueba si el entero siguiente es menor al que se está creando,
        de serlo se cambia la poscición entre el valor actual si el siguiente*/
        while (siguiente != NULL && siguiente -> dato < numero) {
            actual = siguiente;
            siguiente = siguiente -> siguiente;
        }
        /* Se comprueba si el valor del siguiente es igual al del primero,
        de ser así el primero tomará el valor del "Nodo" creado y el siguiente
        "Nodo" el valor del actual*/
        if (siguiente == this -> primero) {
            this -> primero = tmp;
            this -> primero -> siguiente = siguiente;
        } else {
            /* Al ser diferenctes el "Nodo" siguiente tomará el valor
            del que se está creando y el temporal el del siguiente para seguir
            siendo comparado en el ciclo */
            actual -> siguiente = tmp;
            tmp -> siguiente = siguiente;
        }
    }
}
/* Método que genera una salida con el contenido de la lista */
void Lista::imprimir() {
    /* utiliza variable temporal para recorrer la lista. */
    Nodo * tmp = this -> primero;
    /* la recorre mientras sea distinto de NULL (no hay más nodos). */
    cout << (tmp == NULL ? "Lista vacía\n" : "");
    while (tmp != NULL) {
        cout << "[" << tmp -> dato << "] ";
        tmp = tmp -> siguiente;
    }
    cout << endl;
}
