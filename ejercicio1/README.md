# Guía III UI - Ejercicio I

## Descripción del programa 
El programa permite la generación de una Lista dinámica ordenada, utilizando una estructura "Nodo", que posee un tipo de dato, en este caso entero "Int" y una "liga", puntero que puede hace referencia a otro "Nodo", de esta manera, se crean nodos que se apuntan entre sí y son creadon bajo demanada, creando efectivamente una lista dinámica, está se ordenará según se crea, se compará el nodo recién creado con los anteriores en bucle, utilizando nodos auxiliares se va cambinando la "liga" a la que apunta el puntero hasta que quede ordenado, este orden es de números enteros de naturaleza creciente.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./programa
```

## Funcionamiento del programa
El programa Generará una lista según el usuario ingrese valores.
* En el siguiente menú **Debes ingresar un número entero de los que aparecen, es decir, _1 o 0_ y presionar la tecla **enter**.

```
¿Que es lo que desea hacer?
---------------------------
Agregar número entero   [1]
Salir                   [0]
---------------------------
Opción: 
```

### Opción 1 - Agregar número entero
```
Opción: 1 
```
* Esta opción permite agregar valores a la Lista.

```
Opción: 1
Ingrese valor: 15
```
```
Opción: 1
Ingrese valor: -15
```
```
Opción: 1
Ingrese valor: 12
```
* Luego mostrará una salida con los valores ya ordenados de manera ascendente.

```
Valores actuales en su lista: 

[-15] [12] [15] 
```
### Opción 0 - Salir
```
Opción: 0
```
* Esta opción da termino al ciclo principal del programa efectivamente cerrándolo, genera la siguiente salida.

```
Gracias por preferirnos. Hasta pronto!
```

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
