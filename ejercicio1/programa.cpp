#include <iostream>
using namespace std;

/* clases */
#include "Lista.h"
/* Menú con salidas para que el usuario tome una decisión */
void menu(){
        cout << "¿Que es lo que desea hacer?" << endl;
        cout << "---------------------------" << endl;
        cout << "Agregar número entero \t[1]" << endl;
        cout << "Salir \t\t\t[0]" << endl;
        cout << "---------------------------" << endl;
}
/* función principal. */
int main (void) {
    /* Se crean las lista a utilizar */
    Lista *lista = new Lista();
    /* Se declara un booleano "on" como verdadero para el buble principal
    del programa*/
    bool on = true;
    /* "opcion" contendrá lo que el usuario quiere hacer, se usa en la
    estructura switch y desencadena un set de instrucciones según el 
    requerimiento */
    string opcion, dato;
    /* Bucle principal del programa, terminará si el usuario ingresa 0 
    en la variable "opcion", esto cambia el valor de "on" a falso */
        while (on) {
        /* Se crea la lista 3 para ser llenda con los datos de las otras 
        2 listas */
        Lista *lista3 = new Lista();
        /* Se llama al manú para su muestra al usuario, este selecciona una
        alternativa y esta se ejecuta */ 
        menu();
        cout << "Opción: ", getline(cin, opcion);
        switch (stoi(opcion)) {
        /* La "opcion" 1 agrega 1 valor a la lista 1 y lo muestra en 
        pantalla */
        case 1:
            cout << "Ingrese valor: ", getline(cin, dato);
            lista->crear(stoi(dato));
            cout << endl << "Valores actuales en su lista: " << endl<< endl;
            lista->imprimir();
            cout << endl;
            break;
        case 0:
            /* La "opcion" 0 cambia el valor de verdad de la variable "on" 
            de verdadeo a falso */
            cout << "Gracias por preferirnos. Hasta pronto!" << endl;
            on = false;
            break;
        default: 
            /* default le dice al usuario que cometió un error al 
            ingresar la "opción" */
            cout << "Ingrese una selección valida del menú por favor" << endl;
            break;
        }
    }    
    /* libera memoria */
    delete lista;

    return 0;
}