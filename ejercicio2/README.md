# Guía III UI - Ejericio II

## Descripción del programa 
El programa permite la generación de dos Listas dinámicas ordenadas, utilizando una estructura "Nodo", que posee un tipo de dato, en este caso entero "Int" y una "liga", puntero que puede hace referencia a otro "Nodo", de esta manera, se crean nodos que se apuntan entre sí y son creadon bajo demanada, creando efectivamente una lista dinámica, está se ordenará según se crea, se compará el nodo recién creado con los anteriores en bucle, utilizando nodos auxiliares se va cambinando la "liga" a la que apunta el puntero hasta que quede ordenado, este orden es de números enteros de naturaleza creciente, además permite la creación de una **tercera Lista** ordenada utilizando los datos de las otras dos listas creadas.

## Requerimientos para utilizar el programa
* Sistema operativo Linux
* Compilador make

En caso de no contar con make:

### ¿Como instalar make?
Para instalar make, Ud. debe acceder a una terminal dentro del sistema operativo linux e ingresar el siguiente comando:
```
sudo apt-get install make
```
Luego ingresar clave de usuario el sistema procederá a descargar e instalar el compilador. Una vez termine la descarga, su compilador estará listo para usar.

## Compilación y ejecución del programa
* Abrir una terminal en el directorio donde se ubican los archivos del programa e ingresar el siguiente comando.
```
make
```
* Luego de completada la compilación utilice el siguiente comando.
```
./programa
```

## Funcionamiento del programa
El programa toma datos del usuario para crear 2 listas, además con las mismas listas podrá crear una tercera.
* En el siguiente menú **Debes ingresar un número entero de los que aparecen, es decir, _1, 2, 3 o 0_ y presionar la tecla **enter**.

```
El presente programa genera una tercera lista en base a los datos de la lista 1 y 2
¿Que es lo que desea hacer?
---------------------------------------
Agregar número entero a lista 1     [1]
Agregar número entero a lista 2     [2]
Unir lista 1 y 2 para crear lista 3 [3]
Salir                               [0]
---------------------------------------
Opción:  
```

### Opción 1 - Agregar número entero a lista 1
```
Opción: 1 
```
* Esta opción permite agregar valores a la Lista.

```
Opción: 1
Ingrese valor: 15
```
```
Opción: 1
Ingrese valor: -15
```
```
Opción: 1
Ingrese valor: 12
```
* Luego mostrará una salida con los valores ya ordenados de manera ascendente.

```
Valores actuales en su lista 1: 

[-15] [12] [15] 
```
### Opción 2 - Agregar número entero a lista 2 
```
Opción: 2 
```
* Esta opción permite agregar valores a la Lista.

```
Opción: 2
Ingrese valor: 5
```
```
Opción: 2
Ingrese valor: -10
```
* Luego mostrará una salida con los valores ya ordenados de manera ascendente.

```
Valores actuales en su lista 2: 

[5] [10]
```
### Opción 3 - Unir lista 1 y 2 para crear lista 3 
```
Opción: 3 
```
* Esta opción permite crear una tercera lista con los datos contenidos en las otras dos creadas.
```
Opción: 3

Valores actuales en su lista 3: 

[-15] [5] [10] [12] [15] 
```
* Los datos se ordenan al momento de su creación.
### Opción 0 - Salir
```
Opción: 0
```
* Esta opción da termino al ciclo principal del programa efectivamente cerrándolo, genera la siguiente salida.

```
Gracias por preferirnos. Hasta pronto!
```

## Autor
* Dante Aguirre
* Correo: daguirre12@alumnos.utalca.cl
