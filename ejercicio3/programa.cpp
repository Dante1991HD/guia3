#include <iostream>
using namespace std;

/* clases */
#include "Lista.h"
/* Menú con salidas para que el usuario tome una decisión */
void menu(){
        cout << "¿Que es lo que desea hacer?" << endl;
        cout << "--------------------------------------------" << endl;
        cout << "Agregar número entero                    [1]" << endl;
        cout << "Agregar a la listas los enteros faltantes[2]" << endl;
        cout << "Salir                                    [0]" << endl;
        cout << "--------------------------------------------" << endl;
}
/* función principal. */
int main (void) {
    /* Se crean las lista a utilizar */
    Lista *lista = new Lista();
    bool on = true;
    string opcion, dato;
        /* Bucle principal del programa */
        while (on) {
        // Se limpia la terminal para dar un ui más amigable
        // Se llama a menú para mostrar las opciones del programa
        menu();
        /* Se pone el getline en conjunto con la salida 
        para asemejarse a lo pedido por el cliente 
        en el documento del laboratorio*/
        cout << "Opción: ", getline(cin, opcion);
        // Se utiliza la estructura switch para el llamado de los metodos
        switch (stoi(opcion)) {
        case 1:
            /* La "opcion" 1 agrega 1 valor a la lista 1 y lo muestra en 
            pantalla */
            cout << "Ingrese valor: ", getline(cin, dato);
            lista->crear(stoi(dato));
            cout << endl << "Valores actuales en su lista: " << endl<< endl;
            lista->imprimir();
            cout << endl;
            break;
        case 2:
            /* La "opcion" 2 llama al método completar, para que la lista 
            dinamica ordenada posea todos los enteros consecutivos, 
            luego los muestra en pantalla */
            lista->completar();
            cout << endl << "Lista luego de ser completada: " << endl<< endl;
            lista->imprimir();
            cout << endl;
            break;
        case 0:
            /* La "opcion" 0 cambia el valor de verdad de la variable "on" 
            de verdadeo a falso */
            cout << "Gracias por preferirnos. Hasta pronto!" << endl;
            on = false;
            break;
        default: 
            /* default le dice al usuario que cometió un error al 
            ingresar la "opción" */
            cout << "Ingrese una selección valida del menú por favor" << endl;
            break;
        }
    }    
    /* libera memoria */
    delete lista;

    return 0;
}